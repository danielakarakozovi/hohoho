package com.example.santaiscoming

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth

class ProfileActivity : AppCompatActivity() {
    private lateinit var textView:TextView
    private lateinit var textView2:TextView
    private lateinit var  logout_button: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        init()
        registerListeners()
        textView2.text = FirebaseAuth.getInstance().currentUser?.uid
        textView.text = FirebaseAuth.getInstance().currentUser?.email
    }
    private fun init(){
        logout_button = findViewById(R.id.logout_button)
        textView = findViewById(R.id.textView)
        textView2 = findViewById(R.id.textView2)
    }
    private fun registerListeners(){
        logout_button.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            startActivity(Intent(this, LogInActivity::class.java))
            finish()
        }

    }
}