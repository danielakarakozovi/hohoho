package com.example.santaiscoming

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class LogInActivity : AppCompatActivity() {
    private lateinit var EmailLogin: EditText
    private lateinit var PasswordLogIn: EditText
    private lateinit var login: Button
    private lateinit var registration:TextView
    var dog = "@"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(FirebaseAuth.getInstance().currentUser !=null){
            gotoProfile()
        }
        setContentView(R.layout.activity_log_in)
        init()
        registerListeners()
    }

    private fun init() {
        EmailLogin = findViewById(R.id.EmailLogin)
        PasswordLogIn = findViewById(R.id.PasswordLogIn)
        login = findViewById(R.id.login)
        registration=findViewById(R.id.registration)

    }

    private fun registerListeners() {

        registration.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }
        login.setOnClickListener {
            val email = EmailLogin.text.toString()
            val password = PasswordLogIn.text.toString()

            if (email.isEmpty() || password.length<9 ||  password.isEmpty() || dog.isEmpty() || dog.length > 1) {
                Toast.makeText(this, "Empty!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        gotoProfile()
                    } else {
                        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                    }
                }

        }
    }
    private fun gotoProfile() {
        startActivity(Intent(this, ProfileActivity::class.java))
        finish()
    }
}