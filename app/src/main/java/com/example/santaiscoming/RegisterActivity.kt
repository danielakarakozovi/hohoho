package com.example.santaiscoming

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class RegisterActivity : AppCompatActivity() {
    private lateinit var Email: EditText
    private lateinit var Password: EditText
    private lateinit var RepeatPassword: EditText
    private lateinit var reg_button:Button
    private lateinit var login:TextView
    var dog="@"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if(FirebaseAuth.getInstance().currentUser !=null){
            gotoProfile()
        }
        setContentView(R.layout.activity_register)
        init()
        registerListeners()
    }



    private fun init() {
        Email = findViewById(R.id.Email)
        Password = findViewById(R.id.Password)
        RepeatPassword = findViewById(R.id.RepeatPassword)
        reg_button = findViewById(R.id.reg_button)
        login=findViewById(R.id.login)
    }
    private fun registerListeners() {
        login.setOnClickListener {
            val intent = Intent(this, LogInActivity::class.java)
            startActivity(intent)
        }
        reg_button.setOnClickListener {
            val email = Email.text.toString()
            val password = Password.text.toString()
            val secpassword = RepeatPassword.text.toString()

            if (email.isEmpty() || password.length<9 || secpassword.length<9 ||  password.isEmpty() || dog.isEmpty() || dog.length > 1 || secpassword.isEmpty() || password != secpassword) {
                Toast.makeText(this, "Empty!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful){
                        startActivity(Intent(this, ProfileActivity::class.java))
                        finish()
                    }else{
                        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                    }
                }
        }


    }
    private fun gotoProfile() {
        startActivity(Intent(this, ProfileActivity::class.java))
        finish()
    }
}